from udemy.models import UdemyUrls, UdemyDetails

# class ScrapyAppPipeline(object):
#     def process_item(self, item, spider):
#         return item


class UdemycouponlearnviralPipeline(object):

    def process_item(self, item, spider):
        for url in item['urls'][::-1]:
            # Split Url And Code
            if len(url.split("?")) > 1:
                iurl = url.split("?")[0][:-1]
                code = url.split("?")[1][11:]
            else:
                iurl = url.split("?")[0][:-1]
                code = None
            # Create Link With Coupon Code
            uc = UdemyUrls.objects.create(url=iurl, code=code)

            # To get the url last
            if len(iurl.split("/")[-1]) > 0:
                # end without slash
                link = iurl.split("/")[-1]
            else:
                # end with slash
                link = iurl.split("/")[-2]

            udemy = UdemyUrls.objects.get(id=uc.id)
            udemy.link = link + "-" + str(udemy.id)
            udemy.save()

        return item


class UdemySpiderPipeline(object):

    def process_item(self, item, spider):
        data = item
        id = data.pop("id")
        category_list = data.pop("category_list")
        ud = UdemyDetails.objects.create(**data)
        # UdemyDetails To UdemyUrls Reference
        uc = UdemyUrls.objects.get(id=id)
        uc.details = ud
        uc.save()
        return item


class FreeProxyListPipeline(object):

    def process_item(self, item, spider):
        # anonymity = ["anonymous","transparent","elite proxy"]
        # for i in item["data"]:
        #     if i["anonymity"] == "elite proxy":
        #         print(i)
        return item
