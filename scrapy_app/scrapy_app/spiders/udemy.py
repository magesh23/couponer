# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
import ast
import time


class UdemySpider(scrapy.Spider):
    name = 'udemy_spider'
    allowed_domains = ['udemy.com']

    custom_settings = {
        'ITEM_PIPELINES': {
            'scrapy_app.pipelines.UdemySpiderPipeline': 1
        }
    }

    def __init__(self, *args, **kwargs):
        self.course_url = ast.literal_eval(kwargs.get('course_url'))

    def start_requests(self):
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0'}
        yield Request(self.course_url["link"], headers=headers, callback=self.parse)

    def parse(self, response):
        data = {"id": self.course_url["id"]}
        print(response.status)

        # List all meta Tags
        # response.xpath("//meta").extract()
        image = response.xpath("//meta[@property='og:image']/@content").extract()
        data['image'] = image[0]
        title = response.xpath("//meta[@property='og:title']/@content").extract()
        data['title'] = title[0]
        description = response.xpath("//meta[@property='og:description']/@content").extract()
        data['description'] = description[0]

        # ['\nDevelopment\n', '\nProgramming Languages\n', '\nPython\n']
        category_list = response.xpath("//div[@class='main-content']//div[@class='topic-menu  topic-menu--rain-bg ']//a/text()").extract()
        data['category_list'] = category_list

        # Instructor Name   # 'Jose Portilla'
        # author = response.xpath("//div[@class='main-content']//div[@class='full-width full-width--streamer streamer--complete']//div[@class='col-xxs-8 left - col']//span[@data-purpose='instructor - name - top']/a/text()").extract_first().replace("\n","")

        """
        # ['Development']
        category = response.xpath("//meta[@property='udemy_com:category']/@content").extract()
        data['category'] = category
        instructor_url = response.xpath("//meta[@property='udemy_com:instructor']/@content").extract()
        data['instructor_url'] = instructor_url
        price = response.xpath("//meta[@property='udemy_com:price']/@content").extract()
        data['price'] = price
        """

        # What you'll learn
        a = response.xpath("//div[@class='container container--component-margin']//div[@class='what-you-get__content']//ul//span/text()").extract()
        data['whatyouwilllern'] = a
        # Requirements
        b = response.xpath("//div[@class='container container--component-margin']//div[@class='requirements']/div[@class='requirements__content']/ul/li/text()").extract()
        data['requirement'] = b
        # Description
        c = response.xpath("//div[@class='container container--component-margin']//div[@class='js-simple-collapse js-simple-collapse--description']/div[@class='js-simple-collapse-inner']/div")[1].extract().replace(u'\xa0', u' ').replace("\n", "")
        data['desc_para'] = c
        # No of Lectures and Durations
        duration = response.xpath("//div[@class='container container--component-margin']//div[@class='ud-component--clp--curriculum']/div[@class='curriculum-header-container']/div//span/text()").extract()
        # ['\n', '\n', '\n186 lectures\n', '\n', '\n24:10:02\n']

        yield data
