# -*- coding: utf-8 -*-
import scrapy
import time
page_count = 4

# link_urls = {"urls":[]}

class UlvcouponSpider(scrapy.Spider):
    name = 'ulvcoupon'
    allowed_domains = ['udemycoupon.learnviral.com']
    start_urls = ['http://udemycoupon.learnviral.com/page/4']

    custom_settings = {
        'ITEM_PIPELINES': {
            'scrapy_app.pipelines.UdemycouponlearnviralPipeline': 1
        }
    }
    
    def parse(self, response):
        # global link_urls
        datas = {}
        global page_count
        # GET HE URLS FROM PAGE
        datas['urls'] = response.xpath('//*[@class="link-holder"]//a//@href').extract()
        # link_urls["urls"] = link_urls["urls"] + response.xpath('//*[@class="link-holder"]//a//@href').extract()
        # print(link_urls)
        # for url in response.xpath('//*[@class="link-holder"]//a//@href').extract():
        #     datas['url'].append(url)
        # SEND URLS TO PIPELINE
        yield datas
        #time.sleep(10)
        # NEXT PAGE URL
        # next_page_url =  response.xpath('//*[@class="pages"]//a//@href')[page_count].extract()
        # page_count = page_count + 1 if page_count != 0 else page_count + 2

        # Page Reverse
        page_count -= 1
        next_page_url = "https://udemycoupon.learnviral.com/page/" + str(page_count)
        if page_count > 0:
            if next_page_url:
                yield scrapy.Request(next_page_url, callback=self.parse)
        # yield link_urls
        # return link_urls

# # CSV EXPORTER
# # scrapy crawl ucoupon -o ucouponlist1001.csv





"""
# -*- coding: utf-8 -*-
import scrapy
import time

page_count = 0


class UlvcouponSpider(scrapy.Spider):
    name = 'ulvcoupon'
    allowed_domains = ['udemycoupon.learnviral.com']
    start_urls = ['http://udemycoupon.learnviral.com/']

    custom_settings = {
        'ITEM_PIPELINES': {
            'scrapy_app.pipelines.UdemycouponlearnviralPipeline': 1
        }
    }

    def parse(self, response):
        datas = {}
        global page_count
        # GET HE URLS FROM PAGE
        datas['urls'] = response.xpath('//*[@class="link-holder"]//a//@href').extract()

        # for url in response.xpath('//*[@class="link-holder"]//a//@href').extract():
        #     datas['url'].append(url)
        # SEND URLS TO PIPELINE
        yield datas
        time.sleep(2)
        # NEXT PAGE URL
        # next_page_url =  response.xpath('//*[@class="pages"]//a//@href')[page_count].extract()
        page_count = page_count + 1 if page_count != 0 else page_count + 2

        next_page_url = "https://udemycoupon.learnviral.com/page/" + str(page_count)
        if page_count <= 1000:
            if next_page_url:
                yield scrapy.Request(next_page_url, callback=self.parse)
"""
# CSV EXPORTER
# scrapy crawl ucoupon -o ucouponlist1001.csv