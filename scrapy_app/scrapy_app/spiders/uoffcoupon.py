# -*- coding: utf-8 -*-
import scrapy
import time
page_count = 4

# link_urls = {"urls":[]}

class UoffcouponSpider(scrapy.Spider):
    name = 'uoffcoupon_spider'
    allowed_domains = ['udemyoff.com']
    start_urls = ['https://www.udemyoff.com/udemy-free-courses/page/4']

    custom_settings = {
        'ITEM_PIPELINES': {
            'scrapy_app.pipelines.UdemyoffPipeline': 1
        }
    }

    def parse(self, response):
        # global link_urls
        global page_count
        for i in response.xpath("//h2[@class='blog-entry-title entry-title']/a/@href").extract():
            yield scrapy.Request(i, callback=self.parse_inside_url)

        # Page Reverse
        page_count -= 1
        next_page_url = "https://www.udemyoff.com/udemy-free-courses/page/" + str(page_count)
        if page_count > 0:
            if next_page_url:
                yield scrapy.Request(next_page_url, callback=self.parse)

    def parse_inside_url(self, response):
        yield {"url" : response.xpath("//div[@class='wp-block-button aligncenter is-style-squared']/a/@href").extract()[0]}
