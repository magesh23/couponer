import scrapy


class FreeProxyListSpider(scrapy.Spider):
    name = 'free_proxy_list_spider'
    allowed_domains = ['free-proxy-list.net']
    start_urls = ['https://free-proxy-list.net']

    custom_settings = {
        'ITEM_PIPELINES': {
            'scrapy_app.pipelines.FreeProxyListPipeline': 1
        }
    }
    
    def parse(self, response):
        all_data = {"data": [] }
        all_proxy = response.xpath("//div[@class='container']/div[@class='table-responsive']//table/tbody/tr").extract()
        for i in all_proxy:
            selector = scrapy.Selector(text=i)
            ip_rec = selector.xpath("//td/text()").extract()
            # anonymity = ["anonymous","transparent","elite proxy"]
            data ={
                "ip": ip_rec[0],
                "port": ip_rec[1],
                "country_code": ip_rec[2],
                "country": ip_rec[3],
                "anonymity": ip_rec[4],
                "google": ip_rec[5],
                "https": ip_rec[6],
                "last_checked": ip_rec[7],
            }
            all_data["data"].append(data)
        yield all_data
