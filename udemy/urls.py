from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from udemy import views
from django.urls import path
from udemy import views as u


# router = DefaultRouter()
# router.register(r'api/courses', u.UdemyCoursesViewSet)
# router.register(r'api/course/details', u.UdemyCourseDetailsViewSet)

urlpatterns = [
    # url(r'^', include(router.urls)),
    # path('course/<title>/', u.UdemyCourse.as_view(), name='Udemy-Course'),

    # Home
    path('', views.Pages.as_view(), name='Courses_main'),
    # PAGE NUMBER
    path('<page_no>', views.Pages.as_view(), name='Courses_main'),
    url(r'^courses/(?:(?P<page_no>\d+)/)?$', views.Pages.as_view(), name='Courses'),
    # url(r'^course/(?P<course_id>\d+)/$', views.course.as_view(), name='course'),
    # FULL DETAIL
    path('course/<course_detail>', views.CourseDetails.as_view(), name='course'),
    # SUBMIT COURSE
    path('submit_course/', views.submit_course, name='submit_course'),
    # T_P_D
    path('terms_condition/', views.terms_condition, name='terms_and_condition'),
    path('privacy_policy/', views.privacy_policy, name='privacy_policy'),
    path('disclaimer/', views.disclaimer, name='disclaimer'),
]
