from rest_framework import serializers
from udemy.models import UdemyUrls


class UdemyCoursesSerializer(serializers.ModelSerializer):
    course = serializers.CharField(source='id', read_only=True)
    title = serializers.CharField(source='details.title', read_only=True)
    description = serializers.CharField(source='details.description', read_only=True)
    image = serializers.CharField(source='details.image', read_only=True)
    url = serializers.SerializerMethodField(read_only=True)

    def get_url(self, obj):
        return obj.details.title# + "/" + str(obj.id)

    class Meta:
        model = UdemyUrls
        fields = ('title',"description","image","course","url")


class UdemyCourseDetailsSerializer(serializers.ModelSerializer):
    title = serializers.CharField(source='details.title', read_only=True)
    description = serializers.CharField(source='details.description', read_only=True)
    image = serializers.CharField(source='details.image', read_only=True)
    whatyouwilllern = serializers.CharField(source='details.whatyouwilllern', read_only=True)
    requirement = serializers.CharField(source='details.requirement', read_only=True)
    desc_para = serializers.CharField(source='details.desc_para', read_only=True)
    course = serializers.SerializerMethodField(read_only=True)

    def get_course(self, obj):
        return obj.url + "/?couponCode=" + "" if obj.code is None else str(obj.code)

    class Meta:
        model = UdemyUrls
        fields = ('title',"description","image","whatyouwilllern","requirement","desc_para","course")
