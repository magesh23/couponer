from scrapyd_api import ScrapydAPI
# Models
from udemy.models import UdemyUrls
# Django
from django.db.models import Q
# Celery
from celery.task import periodic_task
from celery.schedules import crontab, timedelta
from celery import shared_task


@periodic_task(run_every=crontab(hour='6,18'))
def run_ulvcoupon():
    scrapyd = ScrapydAPI("http://127.0.0.1:6800/")
    scrapyd.schedule('default', 'ulvcoupon')
    return "Couponcode Scraper started"


def run_udemy_spider():
    scrapyd = ScrapydAPI("http://127.0.0.1:6800/")
    for i in UdemyUrls.objects.filter(details=None):
        # Check The Url Detail Alread Exist and ReUse It
        links = UdemyUrls.objects.filter(~Q(details=None), url=i.url)
        if len(links) == 0:
            # New Url
            link_detail ={
                "id": i.id,
                "link": i.url,
            }
            scrapyd.schedule('default', 'udemy_spider', course_url=str(link_detail))
        else:
            # Detail Exits
            i.details = links[0].details
            i.save()
    return "Udemy Spider Sctarted"


def free_proxy_list_spider():
    scrapyd = ScrapydAPI("http://127.0.0.1:6800/")
    scrapyd.schedule('default', 'free_proxy_list_spider')
    return "Free Proxy List Spider Started"


# @periodic_task(run_every=(crontab(minute='*/1')))
# @periodic_task(run_every=timedelta(seconds=10))
# def some_task():
#     # do something
#     print("I'm Running")
#
# @shared_task
# def task_number_one():
#     print("Celery Task Working...")
