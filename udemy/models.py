from django.db import models

# Create your models here.

class UdemyDetails(models.Model):

    title = models.TextField(null=True)
    description = models.TextField(null=True)
    author = models.CharField(max_length=100, null=True)
    # Link
    # url = models.TextField()
    # Image Link
    image = models.TextField(null=True)
    # What You Will Lern
    whatyouwilllern = models.TextField(null=True)
    # Requirement
    requirement = models.TextField(null=True)
    # Description Course Details
    desc_para = models.TextField(null=True)

    class Meta:
        db_table = "udemy_details"


class UdemyUrls(models.Model):

    # Link
    url = models.TextField()
    link = models.TextField(null=True)
    code = models.CharField(max_length=120, null=True)
    is_free = models.BooleanField(default=False)
    no_of_clicks = models.IntegerField(null=True)
    details = models.ForeignKey('UdemyDetails', on_delete=models.CASCADE, related_name='udemy_course_urls',
                                  null=True)

    class Meta:
        db_table = "udemy_urls"


class SubmitedCourse(models.Model):
    name = models.CharField(max_length=100, null=True)
    email = models.EmailField(null=True)
    coupon_code = models.CharField(max_length=100, null=True)
    u_links = models.ForeignKey('UdemyUrls', on_delete=models.CASCADE, null=True, related_name='submited_courses_records')

    class Meta:
        db_table = "submited_course"
