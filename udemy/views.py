from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import TemplateHTMLRenderer
from django.http import HttpResponseRedirect
# from django.http import HttpResponseRedirect
from udemy.models import UdemyUrls, SubmitedCourse
from udemy.serializers import UdemyCoursesSerializer, UdemyCourseDetailsSerializer

import logging
logger = logging.getLogger(__name__)


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'limit'
    max_page_size = 1000

    class Meta:
        ordering = ['-id']


class UdemyCoursesViewSet(viewsets.ModelViewSet):
    queryset = UdemyUrls.objects.all()
    serializer_class = UdemyCoursesSerializer
    pagination_class = StandardResultsSetPagination


class UdemyCourseDetailsViewSet(viewsets.ModelViewSet):
    queryset = UdemyUrls.objects.all()
    serializer_class = UdemyCourseDetailsSerializer

    def get_queryset(self):
        a = self.request.query_params.get('id', None)

        return UdemyUrls.objects.filter(id = a)


class UdemyCourse(APIView):

    def get(self, request, title=None):
        try:
            data = UdemyUrls.objects.filter(url__contains=title)
            li ={
                "title": data[0].details.title,
                "description": data[0].details.description,
                "image": data[0].details.image,
                "whatyouwilllern": data[0].details.whatyouwilllern,
                "requirement": data[0].details.requirement,
                "desc_para": data[0].details.desc_para,
                "course": data[0].url + "/?couponCode=" + "" if data[0].code is None else str(data[0].code),
            }
            return Response(data=li, status=status.HTTP_200_OK)
        except Exception as e:
            logger.error(str(e))
            return Response(data=e, status=status.HTTP_404_NOT_FOUND)


class Pages(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'template/pages.html'

    def get(self, request, page_no=None):
        if page_no is not None:
            page_no = int(page_no)
        else:
            page_no = 1

        queryset = UdemyUrls.objects.exclude(details=None)
        # Number Of Pages
        end = int(len(queryset)/10)
        page_nav = list(range(1, page_no + 1))[-2:-1] + list(range(page_no + 1, end))[:3]

        # queryset = UdemyLinks.objects.filter().order_by("-id")[page_no * 10 - 10:page_no * 10]
        queryset = queryset.order_by("-id")[page_no * 10 - 10:page_no * 10]
        return Response({'UdemyLinks': queryset, 'page_no': page_no+1, 'page_nav': page_nav})

class course(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'template/course.html'

    def get(self, request, course_id=None):
        course_id = int(course_id) - 19
        queryset = UdemyUrls.objects.get(id=course_id)
        print(queryset)
        return Response({'UdemyLinks': queryset})


class CourseDetails(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'template/course.html'

    def get(self, request, course_detail=None):
        if course_detail is not None:
            i = course_detail.split('-')[-1]
            queryset = UdemyUrls.objects.get(id=i)
        return Response({'UdemyLinks': queryset})


def submit_course(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        print(request.POST.dict())
        data = request.POST.dict()
        # To get the url last
        if len(data['course_url'].split("/")[-1]) > 0:
            # end without slash
            link = data['course_url'].split("/")[-1]
        else:
            # end with slash
            link = data['course_url'].split("/")[-2]
        # Submit Course
        udemy = UdemyUrls.objects.create(url=data['course_url'], code=data['coupon_code'])
        udemy = UdemyUrls.objects.get(id=udemy.id)
        udemy.link = link + "-" + str(udemy.id)
        udemy.save()
        # Submit the recoder
        course = SubmitedCourse.objects.create(
            name = data['name'],
            email = data['email'],
            coupon_code = data['coupon_code'],
            u_links_id = udemy.id
        )
        # redirect to a new URL:
        return HttpResponseRedirect('/submit_course/')

    # if a GET (or any other method) we'll create a blank form
    return render(request, 'template/submit.html', {'form': "data"})


def terms_condition(request):
    return render(request, 'template/info/terms_and_conditon.html')


def privacy_policy(request):
    return render(request, 'template/info/privacy_policy.html')


def disclaimer(request):
    return render(request, 'template/info/disclaimer.html')
