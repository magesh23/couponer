from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'couponer.settings')
app = Celery('couponer')

# Using a string here means the worker will not have to
# pickle the object when using Windows.
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')
# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
# app.conf.timezone = 'Asia/Kolkata'

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
